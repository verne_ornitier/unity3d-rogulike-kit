using System;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Represents the eight grid directions and the deltaX, deltaY values associated
 * with those directions.
 *
 * The grid referenced has x positive to the right and y positive downwards.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class Direction
    {
        /**
         * Prevents any instances from being created
         */
        private Direction()
        {
            
        }

        public enum DIR
        {
            UP, DOWN, LEFT, RIGHT, UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT, NONE
        }

        public static Vector2 UP = new Vector2(0, -1);
        public static Vector2 DOWN = new Vector2(0, 1);
        public static Vector2 LEFT = new Vector2(-1, 0);
        public static Vector2 RIGHT = new Vector2(1, 0);

        public static Vector2 UP_LEFT = new Vector2(-1, -1);
        public static Vector2 UP_RIGHT = new Vector2(1, -1);
        public static Vector2 DOWN_LEFT = new Vector2(-1, 1);
        public static Vector2 DOWN_RIGHT = new Vector2(1, 1);

        public static Vector2 NONE = new Vector2(0,0);

        /**
         * An array which holds only the four cardinal directions.
         */
        public static Vector2[] CARDINALS = {UP, DOWN, LEFT, RIGHT};
        /**
         * An array which holds only the four diagonal directions.
         */
        public static Vector2[] DIAGONALS = {UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT};
        /**
         * An array which holds all eight OUTWARDS directions.
         */
        public static Vector2[] OUTWARDS = { UP, DOWN, LEFT, RIGHT, UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT };

        /**
         * Returns the direction that most closely matches the input.
         *
         * @param x
         * @param y
         * @return
         */
        static public DIR getDirection(int x, int y)
        {
            if (x < 0)
            {
                if (y < 0)
                {
                    return DIR.UP_LEFT;
                }
                else if (y == 0)
                {
                    return DIR.LEFT;
                }
                else
                {
                    return DIR.DOWN_LEFT;
                }
            }
            else if (x == 0)
            {
                if (y < 0)
                {
                    return DIR.UP;
                }
                else if (y == 0)
                {
                    return DIR.NONE;
                }
                else
                {
                    return DIR.DOWN;
                }
            }
            else
            {
                if (y < 0)
                {
                    return DIR.UP_RIGHT;
                }
                else if (y == 0)
                {
                    return DIR.RIGHT;
                }
                else
                {
                    return DIR.DOWN_RIGHT;
                }
            }
        }

        /**
         * Returns the direction that most closely matches the input.
         *
         * @param direction
         * @return
         */
        static public DIR getDirection(Vector2 direction)
        {
            int x = Convert.ToInt32(direction.x);
            int y = Convert.ToInt32(direction.y);
            return getDirection(x, y);
        }

        /**
         * Returns the direction that matches the input.
         *
         * @param direction
         * @return
         */
        static public Vector2 getDirection(DIR direction)
        {
            switch (direction)
            {
                case DIR.UP:
                    return UP;
                case DIR.DOWN:
                    return DOWN;
                case DIR.LEFT:
                    return LEFT;
                case DIR.RIGHT:
                    return RIGHT;
                case DIR.UP_LEFT:
                    return UP_LEFT;
                case DIR.UP_RIGHT:
                    return UP_RIGHT;
                case DIR.DOWN_LEFT:
                    return DOWN_LEFT;
                case DIR.DOWN_RIGHT:
                    return DOWN_RIGHT;
                case DIR.NONE:
                default:
                    return NONE;
            }
        }

        /**
         * Returns the Direction one step clockwise including diagonals.
         *
         * @param direction
         * @return
         */
        static public DIR clockwise(Vector2 direction)
        {
            switch (getDirection(direction))
            {
                case DIR.UP:
                    return DIR.UP_RIGHT;
                case DIR.DOWN:
                    return DIR.DOWN_LEFT;
                case DIR.LEFT:
                    return DIR.UP_LEFT;
                case DIR.RIGHT:
                    return DIR.DOWN_RIGHT;
                case DIR.UP_LEFT:
                    return DIR.UP;
                case DIR.UP_RIGHT:
                    return DIR.RIGHT;
                case DIR.DOWN_LEFT:
                    return DIR.LEFT;
                case DIR.DOWN_RIGHT:
                    return DIR.DOWN;
                case DIR.NONE:
                default:
                    return DIR.NONE;
            }
        }

        /**
         * Returns the Direction one step clockwise including diagonals.
         *
         * @param direction
         * @return
         */
        static public DIR clockwise(DIR direction)
        {
            switch (direction)
            {
                case DIR.UP:
                    return DIR.UP_RIGHT;
                case DIR.DOWN:
                    return DIR.DOWN_LEFT;
                case DIR.LEFT:
                    return DIR.UP_LEFT;
                case DIR.RIGHT:
                    return DIR.DOWN_RIGHT;
                case DIR.UP_LEFT:
                    return DIR.UP;
                case DIR.UP_RIGHT:
                    return DIR.RIGHT;
                case DIR.DOWN_LEFT:
                    return DIR.LEFT;
                case DIR.DOWN_RIGHT:
                    return DIR.DOWN;
                case DIR.NONE:
                default:
                    return DIR.NONE;
            }
        }

        /**
         * Returns the Direction one step counterclockwise including diagonals.
         *
         * @param direction
         * @return
         */
        static public DIR counterClockwise(Vector2 direction)
        {
            switch (getDirection(direction))
            {
                case DIR.UP:
                    return DIR.UP_LEFT;
                case DIR.DOWN:
                    return DIR.DOWN_RIGHT;
                case DIR.LEFT:
                    return DIR.DOWN_LEFT;
                case DIR.RIGHT:
                    return DIR.UP_RIGHT;
                case DIR.UP_LEFT:
                    return DIR.LEFT;
                case DIR.UP_RIGHT:
                    return DIR.UP;
                case DIR.DOWN_LEFT:
                    return DIR.DOWN;
                case DIR.DOWN_RIGHT:
                    return DIR.RIGHT;
                case DIR.NONE:
                default:
                    return DIR.NONE;
            }
        }

        /**
         * Returns the Direction one step counterclockwise including diagonals.
         *
         * @param direction
         * @return
         */
        static public DIR counterClockwise(DIR direction)
        {
            switch (direction)
            {
                case DIR.UP:
                    return DIR.UP_LEFT;
                case DIR.DOWN:
                    return DIR.DOWN_RIGHT;
                case DIR.LEFT:
                    return DIR.DOWN_LEFT;
                case DIR.RIGHT:
                    return DIR.UP_RIGHT;
                case DIR.UP_LEFT:
                    return DIR.LEFT;
                case DIR.UP_RIGHT:
                    return DIR.UP;
                case DIR.DOWN_LEFT:
                    return DIR.DOWN;
                case DIR.DOWN_RIGHT:
                    return DIR.RIGHT;
                case DIR.NONE:
                default:
                    return DIR.NONE;
            }
        }

        /**
         * Returns the direction directly opposite of this one.
         *
         * @param direction
         * @return
         */
        static public DIR opposite(Vector2 direction)
        {
            switch (getDirection(direction))
            {
                case DIR.UP:
                    return DIR.DOWN;
                case DIR.DOWN:
                    return DIR.UP;
                case DIR.LEFT:
                    return DIR.RIGHT;
                case DIR.RIGHT:
                    return DIR.LEFT;
                case DIR.UP_LEFT:
                    return DIR.DOWN_RIGHT;
                case DIR.UP_RIGHT:
                    return DIR.DOWN_LEFT;
                case DIR.DOWN_LEFT:
                    return DIR.UP_RIGHT;
                case DIR.DOWN_RIGHT:
                    return DIR.UP_LEFT;
                case DIR.NONE:
                default:
                    return DIR.NONE;
            }
        }

        /**
         * Returns the direction directly opposite of this one.
         *
         * @param direction
         * @return
         */
        static public DIR opposite(DIR direction)
        {
            switch (direction)
            {
                case DIR.UP:
                    return DIR.DOWN;
                case DIR.DOWN:
                    return DIR.UP;
                case DIR.LEFT:
                    return DIR.RIGHT;
                case DIR.RIGHT:
                    return DIR.LEFT;
                case DIR.UP_LEFT:
                    return DIR.DOWN_RIGHT;
                case DIR.UP_RIGHT:
                    return DIR.DOWN_LEFT;
                case DIR.DOWN_LEFT:
                    return DIR.UP_RIGHT;
                case DIR.DOWN_RIGHT:
                    return DIR.UP_LEFT;
                case DIR.NONE:
                default:
                    return DIR.NONE;
            }
        }
    }
}
