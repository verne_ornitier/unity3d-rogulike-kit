using System;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Basic radius strategy implementations.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class BasicRadiusStrategy : RadiusStrategy
    {
        public enum STRATEGY
        {
            /**
             * In an unobstructed area the FOV would be a square.
             *
             * This is the shape that would represent movement radius in an 8-way
             * movement scheme with no additional cost for diagonal movement.
             */
            SQUARE,
            /**
             * In an unobstructed area the FOV would be a diamond.
             *
             * This is the shape that would represent movement radius in a 4-way
             * movement scheme.
             */
            DIAMOND,
            /**
             * In an unobstructed area the FOV would be a circle.
             *
             * This is the shape that would represent movement radius in an 8-way
             * movement scheme with all movement cost the same based on distance from
             * the source
             */
            CIRCLE
        };

        private STRATEGY strategy;

        public STRATEGY Strategy { get { return strategy; } }

        public BasicRadiusStrategy(STRATEGY strategy)
        {
            this.strategy = strategy;
        }

        public float radius(int startx, int starty, int endx, int endy)
        {
            return radius((float)startx, (float)starty, (float)endx, (float)endy);
        }

        public float radius(float startx, float starty, float endx, float endy)
        {
            float dx = Math.Abs(startx - endx);
            float dy = Math.Abs(starty - endy);
            return radius(dx, dy);
        }

        public float radius(int dx, int dy)
        {
            return radius((float)dx, (float)dy);
        }

        public float radius(float dx, float dy)
        {
            dx = Math.Abs(dx);
            dy = Math.Abs(dy);
            float radius = 0f;
            switch (strategy)
            {
                case STRATEGY.SQUARE:
                    radius = Math.Max(dx, dy);//radius is longest axial distance
                    break;
                case STRATEGY.DIAMOND:
                    radius = dx + dy;//radius is the manhattan distance
                    break;
                case STRATEGY.CIRCLE:
                    radius = (float)Math.Sqrt(dx * dx + dy * dy);//standard circular radius
                    break;
            }
            return radius;
        }
    }
}
