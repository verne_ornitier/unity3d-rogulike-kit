using System;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Uses the Elias line running to raycast.
 *
 * Does not currently support translucency.
 * 
 * For information on the sideview parameter, see the EliasLOS documentation.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class EliasFOV : FOVSolver
    {
        private float[,] lightMap, resistanceMap;
        private float maxRadius, force, decay;
        private int width, height;
        private RadiusStrategy rStrat;
        private float sideview = 0.75f;

        /**
         * Creates a solver which will use the default sideview on the internal
         * EliasLOS solver.
         */
        public EliasFOV()
        {
        }

        /**
         * Creates a solver which will use the provided sideview value on the
         * internal EliasLOS solver.
         *
         * @param sideview
         */
        public EliasFOV(float sideview)
        {
            this.sideview = sideview;
        }

        public float[,] calculateFOV(float[,] resistanceMap, int startx, int starty, float force, float decay, RadiusStrategy radiusStrategy)
        {
            this.resistanceMap = resistanceMap;
            width = resistanceMap.Length;
            height = resistanceMap.GetLength(0);
            lightMap = new float[width,height];
            this.force = force;
            this.decay = decay;
            rStrat = radiusStrategy;

            maxRadius = force/decay;
            int left = (int) Math.Max(0, startx - maxRadius - 1);
            int right = (int) Math.Min(width - 1, startx + maxRadius + 1);
            int top = (int) Math.Max(0, starty - maxRadius - 1);
            int bottom = (int) Math.Min(height - 1, starty + maxRadius + 1);


            //run rays out to edges
            for (int x = left; x <= right; x++)
            {
                runLineGroup(startx, starty, x, top);
                runLineGroup(startx, starty, x, bottom);
            }
            for (int y = top; y <= bottom; y++)
            {
                runLineGroup(startx, starty, left, y);
                runLineGroup(startx, starty, right, y);
            }

            return lightMap;
        }

        private void runLineGroup(int startx, int starty, int endx, int endy)
        {
            var tempMap = Elias.LightMap(startx, starty, endx, endy);
            var los = new EliasLOS(sideview); 
            //        boolean xpositive = endx > startx;
            //        boolean ypositive = endy > starty;
            for (int x = Math.Min(startx, endx); x <= Math.Max(startx, endx); x++)
            {
                for (int y = Math.Min(starty, endy); y <= Math.Max(starty, endy); y++)
                {
                    float radius = rStrat.radius(startx, starty, x, y);
                    if (radius < maxRadius && los.isReachable(resistanceMap, startx, starty, x, y))
                    {
                        lightMap[x,y] = Math.Max(lightMap[x,y], tempMap[x,y]*(force - radius*decay));
                    }
                }
            }
        }

        public float[,] calculateFOV(float[,] resistanceMap, int startx, int starty, float radius)
        {
            return calculateFOV(resistanceMap, startx, starty, 1, 1 / radius, new BasicRadiusStrategy(BasicRadiusStrategy.STRATEGY.CIRCLE));
        }
    }
}
