using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * This class merges the results from two or more FOVSolvers.
 * 
 * Currently a work in progress.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class MergedFOV : FOVSolver
    {
        public enum MergeType
        {
            /**
             * The weight will be used to average out all light maps.
             */
            WEIGHTED_AVERAGE,
            /**
             * The weight will be used to average out all light maps, but if any
             * light map shows a cell to have no light then it will be marked as
             * unlit.
             */
            AND,
            /**
             * The maximum light from the light maps will be used. The weight is
             * ignored.
             */
            MAXIMUM,
            /**
             * The minimum light from the light maps will be used. The weight is
             * ignored.
             */
            MINIMUM
        };

        private MergeType mergeType;
        private float totalWeight;

        public MergedFOV(MergeType mergeType)
        {
            this.mergeType = mergeType;
        }

        public float[,] calculateFOV(float[,] resistanceMap, int startx, int starty, float force, float decay, RadiusStrategy radiusStrategy)
        {
            throw new System.InvalidOperationException("Not supported yet.");
        }

        public float[,] calculateFOV(float[,] resistanceMap, int startx, int starty, float radius)
        {
            throw new System.InvalidOperationException("Not supported yet.");
        }

        public float[,] getLight(List<float[,]> maps)
        {
            int width = maps[0].Length;
            int height = maps[0].GetLength(0);
            var result = new float[width,height];
            
            for(int z = 0;z<maps.Count;z++){
                for(int x = 0;x<width;x++){
                    for(int y = 0;y<height;y++){
                        switch (mergeType)
                        {
                            case MergeType.WEIGHTED_AVERAGE:
                               
                                break;
                            case MergeType.AND:
                                
                                break;
                            case MergeType.MAXIMUM:
                                
                                break;
                            case MergeType.MINIMUM: 
                               
                                break;
                        }
                    }
                }
            }
            
            return result;
        }
    }
}
