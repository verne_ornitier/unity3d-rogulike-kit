using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Provides utilities for working with colors as well as caching operations for
 * color creation.
 *
 * All returned SColor objects are cached so multiple requests for the same
 * SColor will not create duplicate long term objects.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class SColorFactory
    {
        private static Dictionary<string, SColor> nameLookup = new Dictionary<string, SColor>();
        private static Dictionary<int, SColor> valueLookup = new Dictionary<int, SColor>();
        private static RNG rng = new RNG();
        private static Dictionary<int, SColor> colorBag = new Dictionary<int, SColor>();
        private static Dictionary<string, List<SColor>> pallets = new Dictionary<string, List<SColor>>();
        private static int floor = 1;//what multiple to floor rgb values to in order to reduce total colors

        /**
         * Prevents any instances from being created
         */
        private SColorFactory()
        {
            
        }

        /**
         * Returns the SColor Constant who's name is the one provided. If one cannot
         * be found then null is returned.
         *
         * This method constructs a list of the SColor constants the first time it
         * is called.
         *
         * @param s
         * @return
         */

        public static SColor colorForName(string s)
        {
            if (nameLookup.Count == 0)
            {
                SColor[] full_pallet = SColor.FULL_PALLET;
                for (var i = 0; i < full_pallet.Length; i++)
                {
                    var sc = full_pallet[i];
                    nameLookup.Add(sc.Name, sc);
                }
            }

            SColor value;
            nameLookup.TryGetValue(s, out value);
            return value;
        }

        /**
         * Returns the SColor who's value matches the one passed in. If no SColor
         * Constant matches that value then a cached or new SColor is returned that
         * matches the provided value.
         *
         * This method constructs a list of the SColor constants the first time it
         * is called.
         *
         * @param rgb
         * @return
         */
        public static SColor colorForValue(int rgb) {
            if (valueLookup.Count == 0) {
                SColor[] full_pallet = SColor.FULL_PALLET;
                for (var i = 0; i < full_pallet.Length; i++)
                {
                    var sc = full_pallet[i];
                    valueLookup.Add(sc.getHex(), sc);
                }
            }

            SColor value;
            valueLookup.TryGetValue(rgb, out value);
            return value;
        }

        /**
         * Returns the number of SColor objects currently cached.
         *
         * @return
         */
        public static int quantityCached()
        {
            return colorBag.Count;
        }

        /**
         * Utility method to blend the two colors by the amount passed in as the
         * coefficient.
         *
         * @param a
         * @param b
         * @param coef
         * @return
         */
        private static int blend(int a, int b, double coef)
        {
            coef = Math.Min(1, coef);
            coef = Math.Max(0, coef);
            return (int)(a + (b - a) * coef);
        }

        /**
         * Returns an SColor that is the given distance from the first color to the
         * second color.
         *
         * @param color1 The first color
         * @param color2 The second color
         * @param coef The percent towards the second color, as 0.0 to 1.0
         * @return
         */
        public static SColor blend(SColor color1, SColor color2, double coef)
        {
            return asSColor(blend(color1.r, color2.r, coef),
                    blend(color1.g, color2.g, coef),
                    blend(color1.b, color2.b, coef));
        }

        /**
         * Returns an SColor that is randomly chosen from the color line between the
         * two provided colors from the two provided points.
         *
         * @param color1
         * @param color2
         * @param min The minimum percent towards the second color, as 0.0 to 1.0
         * @param max The maximum percent towards the second color, as 0.0 to 1.0
         * @return
         */
        public static SColor randomBlend(SColor color1, SColor color2, double min, double max)
        {
            return blend(color1, color2, rng.between(min, max));
        }

        /**
         * Clears the backing cache.
         *
         * Should only be used if an extreme number of colors are being created and
         * then not reused, such as when blending different colors in different
         * areas that will not be revisited.
         */
        public static void emptyCache()
        {
            colorBag = new Dictionary<int, SColor>();
        }

        /**
         * Sets the value at which each of the red, green, and blue values will be
         * set to the nearest lower multiple of.
         *
         * For example, a floor value of 5 would mean that each of those values
         * would be considered the nearest lower multiple of 5 when building the
         * colors.
         *
         * If the value passed in is less than 1, then the flooring value is set at
         * 1.
         *
         * @param value
         */
        public static void setFloor(int value)
        {
            floor = Math.Max(1, value);
        }

        /**
         * Returns the cached color that matches the desired rgb value.
         *
         * If the color is not already in the cache, it is created and added to the
         * cache.
         *
         * This method does not check to see if the value is already available as a
         * SColor constant. If such functionality is desired then please use
         * colorForValue(int rgb) instead.
         *
         * @param rgb
         * @return
         */
        public static SColor asSColor(int rgb)
        {
            if (floor != 1)
            {//need to convert to floored values
                int a = (rgb >> 24) & 0xff;
                a -= a % floor;
                int r = (rgb >> 16) & 0xff;
                r -= r % floor;
                int g = (rgb >> 8) & 0xff;
                g -= g % floor;
                int b = rgb & 0xff;
                b -= b % floor;

                //put back together
                rgb = ((a & 0xFF) << 24)
                        | ((r & 0xFF) << 16)
                        | ((g & 0xFF) << 8)
                        | (b & 0xFF);
            }

            if (colorBag.ContainsKey(rgb))
            {
                SColor value;
                colorBag.TryGetValue(rgb, out value);
                return value;
            }
            else
            {
                var color = new SColor(rgb);
                colorBag.Add(rgb, color);
                return color;
            }
        }

        /**
         * Returns an SColor with the given values, with those values clamped
         * between 0 and 255.
         *
         * @param r
         * @param g
         * @param b
         * @return
         */
        public static SColor asSColor(int r, int g, int b)
        {
            r = Math.Min(r, 255);
            r = Math.Max(r, 0);
            g = Math.Min(g, 255);
            g = Math.Max(g, 0);
            b = Math.Min(b, 255);
            b = Math.Max(b, 0);
            return asSColor(r * 256 * 256 + g * 256 + b);
        }

        /**
         * Returns an SColor representation of the provided Color. If there is a
         * named SColor constant that matches the value, then that constant is
         * returned.
         *
         * @param color
         * @return
         */
        public static SColor asSColor(SColor color)
        {
            return colorForValue(color.getHex());
        }

        /**
         * Returns an SColor that is a slightly dimmer version of the provided
         * color.
         *
         * @param color
         * @return
         */
        public static SColor dim(SColor color)
        {
            return blend(color, SColor.BLACK, 0.1);
        }

        /**
         * Returns an SColor that is a somewhat dimmer version of the provided
         * color.
         *
         * @param color
         * @return
         */
        public static SColor dimmer(SColor color)
        {
            return blend(color, SColor.BLACK, 0.3);
        }

        /**
         * Returns an SColor that is a lot darker version of the provided color.
         *
         * @param color
         * @return
         */
        public static SColor dimmest(SColor color)
        {
            return blend(color, SColor.BLACK, 0.7);
        }

        /**
         * Returns an SColor that is a slightly lighter version of the provided
         * color.
         *
         * @param color
         * @return
         */
        public static SColor light(SColor color)
        {
            return blend(color, SColor.WHITE, 0.1);
        }

        /**
         * Returns an SColor that is a somewhat lighter version of the provided
         * color.
         *
         * @param color
         * @return
         */
        public static SColor lighter(SColor color)
        {
            return blend(color, SColor.WHITE, 0.3);
        }

        /**
         * Returns an SColor that is a lot lighter version of the provided color.
         *
         * @param color
         * @return
         */
        public static SColor lightest(SColor color)
        {
            return blend(color, SColor.WHITE, 0.6);
        }

        /**
         * Returns an SColor that is the fully desaturated (greyscale) version of
         * the provided color.
         *
         * @param color
         * @return
         */
        public static SColor desaturated(SColor color)
        {
            int r = color.r;
            int g = color.g;
            int b = color.b;
            int average = (int)(r * 0.299 + g * 0.587 + b * 0.114);

            return asSColor(average, average, average);
        }

        /**
         * Returns an SColor that is the version of the provided color desaturated
         * the given amount.
         *
         * @param color
         * @param percent The percent to desaturate, from 0.0 for none to 1.0 for
         * fully desaturated
         * @return
         */
        public static SColor desaturate(SColor color, double percent)
        {
            return blend(color, desaturated(color), percent);
        }

        /**
         * Returns a list of colors starting at the first color and moving to the
         * second color. The end point colors are included in the list.
         *
         * @param color1
         * @param color2
         * @return
         */
        public static List<SColor> asGradient(SColor color1, SColor color2)
        {
            String name = palletNamer(color1, color2);
            if (pallets.ContainsKey(name))
            {
                List<SColor> value;
                pallets.TryGetValue(name, out value);
                return value;
            }

            //get the gradient
            Queue<Vector3> gradient = Bresenham.line3D(color1.getRGB(), color2.getRGB());
            var ret = new List<SColor>();
            while(gradient.Count > 0)
            {
                var coord = gradient.Dequeue();
                ret.Add(new SColor(coord));
            }

            pallets.Add(name, ret);
            return ret;
        }

        /**
         * Returns the pallet associate with the provided name, or null if there is
         * no such pallet.
         *
         * @param name
         * @return
         */
        public static List<SColor> pallet(String name)
        {
            List<SColor> value;
            pallets.TryGetValue(name, out value);
            return value;
        }

        /**
         * Returns the SColor that is the provided percent towards the end of the
         * pallet. Bounds are checked so as long as there is at least one color in
         * the palette, values below 0 will return the first element and values
         * above 1 will return the last element;
         *
         * If there is no pallette keyed to the provided name, null is returned.
         *
         * @param name
         * @param percent
         * @return
         */
        public static SColor fromPallet(String name, float percent)
        {
            List<SColor> list;
            pallets.TryGetValue(name, out list);
            if (list == null)
            {
                return null;
            }

            int index = Convert.ToInt32(Math.Round(list.Count * percent));//find the index that's the given percent into the gradient
            index = Math.Min(index, list.Count - 1);
            index = Math.Max(index, 0);
            return list[index];
        }

        /**
         * Places the pallet into the cache, along with each of the member colors.
         *
         * @param name
         * @param pallet
         */
        public static void addPallet(String name, List<SColor> pallet)
        {
            var temp = new List<SColor>();

            //make sure all the colors in the pallet are also in the general color cache
            for (var i = 0; i < pallet.Count; i++)
            {
                var sc = pallet[i];
                temp.Add(asSColor(sc.getHex()));
            }

            pallets.Add(name, temp);
        }

        private static String palletNamer(SColor color1, SColor color2)
        {
            return color1.Name + " to " + color2.Name;
        }
    }
}
