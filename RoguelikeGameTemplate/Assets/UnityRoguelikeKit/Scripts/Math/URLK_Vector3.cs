using System;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Generic three dimensional coordinate class.
 *
 * @author Lewis Potter
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class Vector3Util
    {
        /**
         * Prevents any instances from being created
         */
        private Vector3Util()
        {
            
        }

        public static double distance(Vector3 a, Vector3 b)
        {
            return Math.Sqrt(squareDistance(a,b));
        }

        /**
         * Returns the square of the linear distance between this coordinate
         * point and the provided one.
         * 
         * @param other
         * @return 
         */
        public static double squareDistance(Vector3 a, Vector3 b)
        {
            double dx = a.x - b.x;
            double dy = a.y - b.y;
            double dz = a.z - b.z;
            return dx * dx + dy * dy + dz * dz;
        }

        /**
         * Returns the Manhattan distance between this point and the provided one.
         * The Manhattan distance is the distance between each point on each separate
         * axis all added together.
         * 
         * @param other
         * @return 
         */
        public static int manhattanDistance(Vector3 a, Vector3 b)
        {
            var distance = Math.Abs(a.x - b.x);
            distance += Math.Abs(a.y - b.y);
            distance += Math.Abs(a.z - b.z);
            return Convert.ToInt32(distance);
        }

        /**
         * Returns the largest difference between the two points along any one axis.
         * 
         * @param other
         * @return 
         */
        public static int maxAxisDistance(Vector3 a, Vector3 b)
        {
            return Convert.ToInt32(Math.Max(Math.Max(Math.Abs(a.x - b.x), Math.Abs(a.y - b.y)), Math.Abs(a.z - b.z)));
        }
    }
}
