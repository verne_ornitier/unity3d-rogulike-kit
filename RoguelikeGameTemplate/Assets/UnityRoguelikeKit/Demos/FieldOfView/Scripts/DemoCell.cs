using URLK;
using UnityEngine;
using System.Collections;

public class DemoCell
{
    public float resistance;
    public char representation;
    public SColor color;

    /**
     * Creates a new cell which has minimal properties needed to represent
     * it.
     *
     * @param resistance
     * @param light
     * @param representation
     */
    public DemoCell(float resistance, char representation, SColor color)
    {
        this.resistance = resistance;
        this.representation = representation;
        this.color = color;
    }
}
