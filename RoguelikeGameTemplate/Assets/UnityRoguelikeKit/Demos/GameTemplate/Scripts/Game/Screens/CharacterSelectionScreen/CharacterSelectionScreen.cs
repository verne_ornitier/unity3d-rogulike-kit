using System;
using UnityEngine;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public class CharacterSelectionScreen : IScreen
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    private CharacterSelectionScreenData data;
    private GameObject player;
    private int current_selected_character;

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                       C O N S T R U C T O R
    public CharacterSelectionScreen(Game game)
        : base(game)
    {
        data = new CharacterSelectionScreenData();
        current_selected_character = -1;
    }

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public override IScreen set_active()
    {
        //We might have been to a menu screen, only to return to game screen... 
        //so no reason to completely load that scene again!
        if (Application.loadedLevel != data.get_character_selection_screen())
            Application.LoadLevel(data.get_character_selection_screen());

        post_level_load();
        return this;
    }

    public override void update(float elapsed_time)
    {
    }

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    private void post_level_load()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (player == null)
            throw new System.InvalidOperationException("Couldn't find Player tagged GameObject in scene!");
    }

    private void add_character(int id, string name)
    {
        //TODO: Add characters to some form of UI Character List here
    }

    private void on_create_new_character_clicked()
    {
        //TODO: Activate the create new character screen
    }

    private void on_create_new_character_form_clicked()
    {
        //TODO: Process/Validate new character form
    }
	
	private void on_login_selected_character_clicked()
	{
		//TODO: Actually login the selected character.
		
		//Load the game!
		game.change_to_game_screen();
	}

    private void on_select_character(int character_index)
    {
        if (character_index == current_selected_character)
            return;

        //TODO: When shifting through the list of characters, this function should handle to logic of that happening
        current_selected_character = character_index;
    }
}
