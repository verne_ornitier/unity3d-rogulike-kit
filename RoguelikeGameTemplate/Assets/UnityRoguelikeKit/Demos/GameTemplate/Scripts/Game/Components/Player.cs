using UnityEngine;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
[AddComponentMenu("Roguelike Game Template/Game/Player")]
public class Player : MonoBehaviour 
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    private void Start()
	{
	    Game.Instance.start();
	}

    private void Update()
    {
	    Game.Instance.update();
	}

    private void LateUpdate()
    {
        Game.Instance.late_update();
    }
}
