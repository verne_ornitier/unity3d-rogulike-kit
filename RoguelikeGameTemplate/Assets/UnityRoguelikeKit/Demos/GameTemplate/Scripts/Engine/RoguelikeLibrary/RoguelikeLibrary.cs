using UnityEngine;
using System.Collections;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
// This is the main entry point  for the
// library, to update what needs regular
// updating, and generally cause overall
// flow. Not  a  required  class to use, 
// but   should    generally   be   more 
// convenient   if    using   the   full 
// Roguelike Library.
//
public class RoguelikeLibrary
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                       C O N S T R U C T O R

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
}
